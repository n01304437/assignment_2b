﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assifnment2b.user_control
{
    public partial class Web_Programming : System.Web.UI.UserControl
    {

        DataView MyWebProg()
        {
            DataTable webdata = new DataTable();
            DataColumn web_col = new DataColumn();
            DataColumn data_col = new DataColumn();
            DataRow webrow;
            web_col.ColumnName = "Line";
            web_col.DataType = System.Type.GetType("System.Int32");
            webdata.Columns.Add(web_col);
            data_col.ColumnName = "Code";
            data_col.DataType = System.Type.GetType("System.String");
            webdata.Columns.Add(data_col);
            List<string> webProg_code = new List<string>(new string[]{
                "<html>",
                "~<head>",
                "~<title> My web Page</title>",
                "~</head>",
                "~<body>",
                "~~<h1>Welcome to my Website</h1>",
                "~~<h2>Two</h2>",
                "~~<h3>Three</h3>",
                "~~<h4>Four</h4>",
                "~~<h5>Five</h5>",
                "~~<h6>Six</h6>",
                "~~<p>Paragraph tag</p>",
                "~~<p>hello, how<em> are</em> you</p>",
                "~~<p>Here is some content. <br/> Thank you</p>",
                "~</body>",
                "</html>"
            });
            int i = 0;
            foreach (string web_line in webProg_code)
            {
                webrow = webdata.NewRow();
                webrow[web_col.ColumnName] = i;
                string web_formatted = System.Net.WebUtility.HtmlEncode(web_line);
                web_formatted = web_formatted.Replace("~", "&nbsp;&nbsp;&nbsp;");
                webrow[data_col.ColumnName] = web_formatted;

                i++;
                webdata.Rows.Add(webrow);
            }


            DataView codeview = new DataView(webdata);
            return codeview;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds1 = MyWebProg();
            MywebProg.DataSource = ds1;
            MywebProg.DataBind();

        }
    }
}