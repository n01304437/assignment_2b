﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assifnment2b.user_control
{
    public partial class Database : System.Web.UI.UserControl
    {
        DataView Mydatabase()
        {
            DataTable database_data = new DataTable();
            DataColumn database_col = new DataColumn();
            DataColumn Data_col = new DataColumn();
            DataRow databaserow;
            database_col.ColumnName = "Line";
            database_col.DataType = System.Type.GetType("System.Int32");
            database_data.Columns.Add(database_col);
            Data_col.ColumnName = "Code";
            Data_col.DataType = System.Type.GetType("System.String");
            database_data.Columns.Add(Data_col);
            List<string> dataBase_code = new List<string>(new string[]{

               "~  1. We’re looking for the phone number, full address, and last name of a client named",
                "rebecca.She’s either from ILLINOIS, IDAHO, or IOWA, I can’t remember.< br />< br />",
               "~< b > Query:</ b >< br />",
                "~SELECT clientphone, clientcity || ' ' || clientzip || ' ' || clientprov as fulladdress, clientlname from clients<br />",
                "~where (clientfname = 'rebecca') and(clientcity = 'ILLINOIS' or clientcity = 'IDAHO' or clientcity = 'LOWA');< br />< br />",

              "~2.Find the 10 clients who have the oldest accounts with us.< br />< br />",
               "~< b > Query:</ b >< br />",
               "~ SELECT * from clients order by clientjoindate asc fetch first 10 rows only;< br />"



            });
            int i = 0;
            foreach (string db_line in dataBase_code)
            {
                databaserow = database_data.NewRow();
                databaserow[database_col.ColumnName] = i;
                string db_formatted = System.Net.WebUtility.HtmlEncode(db_line);
                db_formatted = db_formatted.Replace("~", "&nbsp;&nbsp;&nbsp;");
                databaserow[Data_col.ColumnName] = db_formatted;

                i++;
                database_data.Rows.Add(databaserow);
            }


            DataView codeview = new DataView(database_data);
            return codeview;
        }


        

        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds1 = Mydatabase();
            MyDataBase.DataSource = ds1;
            MyDataBase.DataBind();


        }
    }
}