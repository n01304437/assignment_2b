﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assifnment2b.user_control
{
    public partial class Digital_Design : System.Web.UI.UserControl
    {

        DataView MyDigitalDesign()
        {
            DataTable digitaldata = new DataTable();
            DataColumn digital_col = new DataColumn();
            DataColumn D_data_col = new DataColumn();
            DataRow digitalrow;
            digital_col.ColumnName = "Line";
            digital_col.DataType = System.Type.GetType("System.Int32");
            digitaldata.Columns.Add(digital_col);
            D_data_col.ColumnName = "Code";
            D_data_col.DataType = System.Type.GetType("System.String");
            digitaldata.Columns.Add(D_data_col);
            List<string> digital_code = new List<string>(new string[]{
                "~<b>Code of table (in HTML) </b><br />",
                "~<table border = 1 cellpadding = 5 cellspacing = 5>",
                "~ <tr>",
                "~<th>Name</th>",
                 "~<th>Salary</th>",
                 "~ </tr>",
                 "~<tr>",
                 "~<td>Ramesh Raman</td>",
                 "~<td>5000</td>",
                 "~</tr>",
                 "~<tr>",
                 "~<td>Shabbir Hussein</td>",
                 "~<td>7000</td>",
                 "~</tr>",
                 "~</table>",
                 "~",
                  "~<b>Code of Table (in CSS) </b>",
                    "~table, td, th {<br />",
                   "~ border: 1px solid black;< br />",
                   "~  }<br />",
                        "~table {<br />",
                            "~border-collapse: collapse;<br />",
                            "~width: 100%;<br />",
                        "~}<br />",
                        "~th {<br />",
                            "~height: 50px;<br />",
                        "~}<br />"
             });
            int i = 0;
            foreach (string digital_line in digital_code)
            {
                digitalrow = digitaldata.NewRow();
                digitalrow[digital_col.ColumnName] = i;
                string digital_formatted = System.Net.WebUtility.HtmlEncode(digital_line);
                digital_formatted = digital_formatted.Replace("~", "&nbsp;&nbsp;&nbsp;");
                digitalrow[D_data_col.ColumnName] = digital_formatted;

                i++;
                digitaldata.Rows.Add(digitalrow);
            }
            DataView D_codeview = new DataView(digitaldata);
            return D_codeview;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds1 = MyDigitalDesign();
            MyDigital.DataSource = ds1;
            MyDigital.DataBind();

        }
    }
}