﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assifnment2b
{
    public partial class Web_Programming : System.Web.UI.Page
    {
        DataView SearchedWebProg()
        {
            DataTable webdata = new DataTable();
            DataColumn web_col = new DataColumn();
            DataColumn data_col = new DataColumn();
            DataRow webrow;
            web_col.ColumnName = "Line";
            web_col.DataType = System.Type.GetType("System.Int32");
            webdata.Columns.Add(web_col);
            data_col.ColumnName = "Code";
            data_col.DataType = System.Type.GetType("System.String");
            webdata.Columns.Add(data_col);
            List<string> webProg_code = new List<string>(new string[]{
                 "~<table border=1px solid style=font-family:Georgia, Garamond, Serif;color:blue;font-style:italic;>",
               "~<tr>",
                  "~<th>Table Header</th>",
                  "~<th>Table Header</th>",
               "~</tr>",
                "~<tr>",
                   "~<td>Table cell 1</td>",
                   "~<td>Table cell 2</td>",
                "~</tr>",
                "~<tr>",
                   "<td>Table cell 3</td>",
                   "~<td>Table cell 4</td>",
                "~</tr>",
                "~</table>"
            });
            int i = 0;
            foreach (string web_line in webProg_code)
            {
                webrow = webdata.NewRow();
                webrow[web_col.ColumnName] = i;
                string web_formatted = System.Net.WebUtility.HtmlEncode(web_line);
                web_formatted = web_formatted.Replace("~", "&nbsp;&nbsp;&nbsp;");
                webrow[data_col.ColumnName] = web_formatted;
                i++;
                webdata.Rows.Add(webrow);
            }
            DataView codeview = new DataView(webdata);
            return codeview;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
          
            DataView ds2 = SearchedWebProg();
            SearchwebProg.DataSource = ds2;
            SearchwebProg.DataBind();

            
        }
    }
}