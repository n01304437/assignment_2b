﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assifnment2b
{
    public partial class Digital_Design : System.Web.UI.Page
    {
          DataView SearchDigitalDesign()
        {
            DataTable digitaldata = new DataTable();
            DataColumn digital_col = new DataColumn();
            DataColumn D_data_col = new DataColumn();
            DataRow digitalrow;
            digital_col.ColumnName = "Line";
            digital_col.DataType = System.Type.GetType("System.Int32");
            digitaldata.Columns.Add(digital_col);
            D_data_col.ColumnName = "Code";
            D_data_col.DataType = System.Type.GetType("System.String");
            digitaldata.Columns.Add(D_data_col);
            List<string> digital_code = new List<string>(new string[]{

                "~<b>Code of Table (in HTML) </b><br /><br />",
                 "~<table style=width:100%>",
                  "~<tr>",
                  "~<th>Firstname</th>",
                "~  <th>Lastname</th>",
                "~ <th>Age</th>",
                "~ </tr>",
                "~<tr>",
                "~<td>Jill</td>",
                "~<td>Smith</td>",
                "~ <td>50</td>",
                "~</tr>",
                "~<tr>",
                "~ <td>Eve</td>",
                "~ <td>Jackson</td>",
                "~ <td>94</td>",
                "~ </tr>",
                "~ <tr>",
                "~<td>John</td>",
                "~<td>Doe</td>",
                "~ <td>80</td>",
                "~</tr>",
                "~</table>",
                "~<br /><br /><b>Code of table (in CSS):</ b >< br />< br />",
                  "~table {< br />",
                    "~border - collapse: collapse;< br />",
                "~}< br />",
                "~table, td, th {< br />",
                  "~border: 1px solid black;< br />",
                "~}< br />"
            });
            int i = 0;
            foreach (string digital_line in digital_code)
            {
                digitalrow = digitaldata.NewRow();
                digitalrow[digital_col.ColumnName] = i;
                string digital_formatted = System.Net.WebUtility.HtmlEncode(digital_line);
                digital_formatted = digital_formatted.Replace("~", "&nbsp;&nbsp;&nbsp;");
                digitalrow[D_data_col.ColumnName] = digital_formatted;

                i++;
                digitaldata.Rows.Add(digitalrow);
            }
            DataView D_codeview = new DataView(digitaldata);
            return D_codeview;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            
            DataView ds2 = SearchDigitalDesign();
            SearchedDigital.DataSource = ds2;
            SearchedDigital.DataBind();


        }
    }
}