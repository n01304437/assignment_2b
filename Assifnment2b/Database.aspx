﻿
<%@ Page Title="database Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assifnment2b.Database" %>

<asp:Content ContentPlaceHolderID="Tricky_Concept" runat="server">
<h2>Tricky concept</h2>
    <div id="trick">
1) How to insert into a table with just one IDENTITY column? <br />
<b>Answer:</b> INSERT INTO tbl_ID DEFAULT VALUES;<br /><br />
2) How to concatenate multiple rows into a single text string without using loop?<br />
Suppose we have a table with the following data:<br /><br />

ClientID    ClientName<br />
3           Sowrabh Malhotra<br />
4           Saji Mon<br />
6           Sajith Kumar<br />
7           Vipin Job<br />
8           Monoj Kumar<br />
We need to concatenate the ClientName column like the following:<br /><br />

Sowrabh Malhotra, Saji Mon, Sajith Kumar, Vipin Job, Monoj Kumar<br />
<b>Answer:</b><br />

<b>Method 1 ---> </b><br />
Hide   Copy Code<br />
SELECT ClientName + ', ' <br />
From ClientMaster <br />
For XML PATH('')<br />
<b>Method 2 ---></b><br />

DECLARE @ClientNames VARCHAR(MAX);<br />
SET @ClientNames = '';<br /><br />

SELECT @ClientNames = @ClientNames + IsNull(ClientName + ', ', '')<br />
FROM ClientMaster <br />
Select @ClientNames<br />

</div>
</asp:Content>
<asp:Content ContentPlaceHolderID="My_Code" runat="server">
    <h2>My Code</h2>

    <DB_uctrl:DB runat="server" id="MyData_Base" />

</asp:Content>
<asp:Content ContentPlaceHolderID="Searched_Code" runat="server">
    <h2>Searched Code</h2>
    <asp:DataGrid ID="SearchDataBase" runat="server" CssClass="database"
        GridLines="None" Width="270px" CellPadding="2" >
        <HeaderStyle Font-Size="Medium" BackColor="#40bf80" ForeColor="White" />
        <ItemStyle BackColor="#ffc266" ForeColor="white"/>     
    </asp:DataGrid>


  
    
</asp:Content>
<asp:Content ContentPlaceHolderID="Links" runat="server">
    <h2>Helpful Links</h2>
     <div id="links">
        <ul id="l1">
           <li><a href="https://academy.vertabelo.com/blog/18-best-online-resources-for-learning-sql-and-database-concepts/">SQL and Database Concept</a></li>
           <li><a href="https://www.mssqltips.com/sqlservertip/1070/simple-script-to-backup-all-sql-server-databases/">SQL server</a></li>
        </ul>
    </div>   
</asp:Content>