﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assifnment2b
{
    public partial class Database : System.Web.UI.Page
    {

        DataView Searchdatabase()
        {
            DataTable database_data = new DataTable();
            DataColumn database_col = new DataColumn();
            DataColumn Data_col = new DataColumn();
            DataRow databaserow;
            database_col.ColumnName = "Line";
            database_col.DataType = System.Type.GetType("System.Int32");
            database_data.Columns.Add(database_col);
            Data_col.ColumnName = "Code";
            Data_col.DataType = System.Type.GetType("System.String");
            database_data.Columns.Add(Data_col);
            List<string> dataBase_code = new List<string>(new string[]{

             "~1.How to display 1 to 100 Numbers with query?<br />",
            "~<b>Query:</b> Select level from dual connect by level <=100;< br />< br />",
            "~2.How to find count of duplicate rows? (95 % asked in SQL queries for Interviews )< br />",
            "~ < b > Query:</ b > Select rollno, count(rollno) from Student<br />",
            "~Group by rollno<br />",
            "~Having count(rollno) > 1 < br />",
            "~Order by count(rollno) desc;< br />"

            });
            int i = 0;
            foreach (string db_line in dataBase_code)
            {
                databaserow = database_data.NewRow();
                databaserow[database_col.ColumnName] = i;
                string db_formatted = System.Net.WebUtility.HtmlEncode(db_line);
                db_formatted = db_formatted.Replace("~", "&nbsp;&nbsp;&nbsp;");
                databaserow[Data_col.ColumnName] = db_formatted;

                i++;
                database_data.Rows.Add(databaserow);
            }


            DataView codeview = new DataView(database_data);
            return codeview;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds2 = Searchdatabase();
            SearchDataBase.DataSource = ds2;
            SearchDataBase.DataBind();


        }
    }
}