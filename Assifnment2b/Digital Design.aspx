﻿

<%@ Page Title="Degital Design Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Digital Design.aspx.cs" Inherits="Assifnment2b.Digital_Design" %>

<asp:Content ContentPlaceHolderID="Tricky_Concept" runat="server">
    <h2>Tricky concept</h2>
    <div id="trick">
                    <b>defination of Table In HTML:</b><br />
							An HTML table is defined with the 
                             <% string a ="<table>";
                                Response.Write(HttpUtility.HtmlEncode(a));
                              %> tag.<br />
							Each table row is defined with the 
                             <% string b ="<tr>";
                                Response.Write(HttpUtility.HtmlEncode(b));
                              %> tag.<br />
							A table header is defined with the 
                             <% string c ="<th>";
                                Response.Write(HttpUtility.HtmlEncode(c));
                              %> tag.<br />
							By default, table headings are bold and centered.<br />
							A table data/cell is defined with the 
                             <% string d ="<td>";
                                Response.Write(HttpUtility.HtmlEncode(d));
                              %> tag.<br />
			

        </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="My_Code" runat="server">
    <h2>My Code</h2>
    <DD_uctrl:D_Design runat="server" id="MyDigital_Design" />
    
</asp:Content>
<asp:Content ContentPlaceHolderID="Searched_Code" runat="server">
    <h2>Searched Code</h2>
    
    <asp:DataGrid ID="SearchedDigital" runat="server" CssClass="digital"
        GridLines="None" Width="270px" CellPadding="2" >
        <HeaderStyle Font-Size="Medium" BackColor="#40bf80" ForeColor="White" />
        <ItemStyle BackColor="#ffc266" ForeColor="white"/>     
    </asp:DataGrid>

</asp:Content>
<asp:Content ContentPlaceHolderID="Links" runat="server">
    <h2>Helpful Links</h2>
     <div id="links">
        <ul id="l1">
           <li><a href="https://jigsaw.w3.org/css-validator/">CSS Validator</a></li>
           <li><a href="https://validator.w3.org/">CSS Validator</a></li>
        </ul>
    </div>   
</asp:Content>