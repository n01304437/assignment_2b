﻿
<%@ Page Title="Web Programming Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Web Programming.aspx.cs" Inherits="Assifnment2b.Web_Programming" %>

<asp:Content ContentPlaceHolderID="Tricky_Concept" runat="server">
<h2>Tricky concept</h2>
    <div id="trick">
       <p>
          <b>What are the different data type of javascript?</b><br />
As per the ECMAScript 5 specification, there are 6 different data types in javascript.<br />

Boolean //true/false/ <br />
<%--Number //1,1.0 <br />--%>
String // 'a', "a" <br />
Null // null <br />
Undefined // undefined<br />
(5 primitive types and 1 non-primitive types) <br />

Object // new Objects()<br />
       </p>
     </div>    
</asp:Content>
<asp:Content ContentPlaceHolderID="My_Code" runat="server">
    <h2>My Code</h2>

    <WP_uctrl:W_Programming runat="server" id="MyWeb_Prog" />
</asp:Content>
<asp:Content ContentPlaceHolderID="Searched_Code" runat="server">
    <h2>Searched Code</h2>
    <asp:DataGrid ID="SearchwebProg" runat="server" CssClass="web"
        GridLines="None" Width="270px" CellPadding="2" >
        <HeaderStyle Font-Size="Medium" BackColor="#40bf80" ForeColor="White" />
        <ItemStyle BackColor="#ffc266" ForeColor="white"/>     
    </asp:DataGrid>

</asp:Content>

<asp:Content ContentPlaceHolderID="Links" runat="server">
    <h2>Helpful Links</h2>
    <div id="links">
        <ul id="l1">
           <li><a href="https://www.techopedia.com/definition/23898/web-programming">Web programming</a></li>
           <li><a href="https://www.html.am/html-codes/">Html Code</a></li>
           <li><a href="http://web-source.net/html_codes_chart.htm#link"> Html Code Chart</a></li>
        </ul>
    </div>
</asp:Content>

  
