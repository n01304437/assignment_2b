﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Assifnment2b._Default" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div id="body">
        <div class="Intro">
            
            <h1>Hi there, I'm Manpreet Kaur</h1>
            <p class="lead">Student of Web Development</p>
        </div>
    </div>

    
        <div class="col-md-12">
            <h2>Study Guide</h2>
            <p>
                Study guides can be broad based to facilitate learning in a number of areas, or be resources that foster comprehension of literature, research topics, 
                history, and other subjects. General topics include study and testing strategies; reading, writing, classroom, and project management skills; as well 
                as techniques for learning as an adult, with disabilities, and online. Some will summarize chapters of novels or the important elements of the subject.
                Study guides for math and science often present problems (as in problem-based learning) and will offer techniques of resolution.
            </p>
           
        </div>
      

</asp:Content>
